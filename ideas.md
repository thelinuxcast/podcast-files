# This is a place to save ideas for the podcast. 

Format: 

* This is my idea -- Matt

(if you're not a part of the pod, and want to suggest a topic, feel free to create a pull request)
---

# Matt

* Linux Tribalism: a good or bad thing -- Matt
- done - * Can Firefox Ever Be Good Again? -- Matt
* Linux YouTuber Tier List -- Matt
- done - * Notes: A Journey to the Perfect Note System
* Is Email Broken?
* What makes a perfect distro?
* Biggest and Smallest FOSS Wins
* What features are missing in Linux?
* If you could change one thing about your workflow/setup what would it be?
* Is there a Linux regret you'd like to share?
* What's the best Linux hardware?
* Can FOSS be trusted?
* How would we fix Ubuntu?
* Is Cloud Native Linux Really the Future? or is it just the future of Fedora?
* The hardest things to do on Linux
* Is choice really the best thing about Linux?

# Drew
# 
- done - I can't live without ...

- done - My top 3 or 5 favorite tools of all time

- done - "My First Distro" stories and why don't I use it any more.

Distro Showdown - Why I'm right and you're wrong.

Guilty Pleasure - I like this but don't judge me (could be a distro or something else)

The Perfect Linux Setup

Mythbusters - Debunking the Biggest Myths Surrounding Linux

- done -Linux Hacks - Little Tricks that Make a Big Difference

Underrated Tools - Hidden Gems in the Linux World

- done - Unpopular Opinions

# Guests We Might Try to Have On

- Distrotube
- Brodie Robertson
- Christ Titus
- TechHut
- Alex AKA The Linux Tube



