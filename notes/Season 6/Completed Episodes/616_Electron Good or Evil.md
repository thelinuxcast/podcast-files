
# Season 6, Episode 16. Episode ID 80

### What have we been up to Linux related this Week? ###

Tyler - I have been playing with Mednafen and getting the streaming script I use working well.

Matt – I joined Mastodon again. 
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt -  https://www.phoronix.com/scan.php?page=news_item&px=Linux-Disable-FDRAWCMD
Tyler - https://www.phoronix.com/scan.php?page=article&item=amd-radeon-rx6400&num=1

---

### Main Topic ### 
Is Electron good or evil?


---

### Apps of the Week ###

Matt - BlueGriffon 

Tyler - helix
