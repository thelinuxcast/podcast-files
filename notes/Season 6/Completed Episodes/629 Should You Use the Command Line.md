
# Season 6, Episode 29. Episode ID 93

### What have we been up to Linux related this Week? ###

Tyler - Finally getting back to work on game dev. Now taking my multiplayer networking course.

Matt – Finally got Samba to work on Fedora. 
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.phoronix.com/news/CUPS-3.0-Architecture-Overhaul 
Tyler - https://www.bleepingcomputer.com/news/security/microsoft-teams-stores-auth-tokens-as-cleartext-in-windows-linux-macs/

---

### Main Topic ### 
Is the Command Line Worth Learning?


---

### Apps of the Week ###

Matt - pcloud 

Tyler - Udemy
