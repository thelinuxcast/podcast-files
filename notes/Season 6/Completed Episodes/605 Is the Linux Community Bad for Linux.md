
# Season 6, Episode 05. Episode ID 69
Topic from Matt.

### What have we been up to Linux related this Week? ###

Tyler - Moved all the way out to Colorado to live with a buddy from Discord. Seriously considering going back to Windows.

Matt – A lot of work. It has been insane. I've also been working on improving my network shares with samba and ssh.
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/elementary-os-7-will-be-based-on-ubuntu-22-04-lts-offer-gtk4-apps-and-power-profiles 
Tyler - https://liliputing.com/2022/02/makers-of-the-jingpad-a1-are-selling-the-linux-tablet-for-45-percent-off-following-staffing-cuts.html

---

### Main Topic ### 
Is the Linux Community Toxic?


---

### Apps of the Week ###

Matt - Tab groups 

Tyler - Yakuake
