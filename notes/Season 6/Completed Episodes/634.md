# Season 6, Episode 34. Episode ID 98

### What have we been up to Linux related this Week?

Tyler - Switched to a MacBook Air, and switched to an iPhone. It's fixed all my issues with Windows and Linux.

Matt – I installed Void and have been using that.

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Tyler on YouTube - https://youtube.com/ZaneyOG

Contact Info at https://thelinuxcast.org/contact

---
# I left these links. We can reuse them if they're still useful. If not, replace with new ones.
* Matt News Links (3 Links)
1. https://www.phoronix.com/news/Fedora-38-Sway-Spin-Proposed
2. https://www.economist.com/business/2022/10/28/elon-musk-buys-twitter-at-last
3. https://www.pcmag.com/news/mastodon-gains-200000-new-users-after-musk-completes-twitter-takeover 

* Tyler News Links (3 Links)
1. https://www.gamingonlinux.com/2022/11/steam-on-chromebook-is-now-in-beta-with-amd-support/
2. https://www.gamingonlinux.com/2022/11/steam-deck-pushed-linux-to-the-highest-share-on-steam-in-years/
3. https://9to5linux.com/system76s-rust-based-cosmic-desktop-promises-hdr-support-smooth-nvidia-experience

---

### Apps of the Week

Matt - mksession in vim 

Tyler - 
