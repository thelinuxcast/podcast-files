# Season 6, Episode 06. Episode ID 70
Topic from Matt.



### What have we been up to Linux related this Week? ###

Tyler - I REALLY DID switch to Gentoo and it's kinda lit.

Matt – I installed Void Linux this week. Meh.
---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Discord https://discord.gg/89FRrrcGhC

Telegram https://t.me/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/kde-plasma-5-24-desktop-environment-officially-released-as-the-next-lts-series 
Tyler - https://9to5linux.com/mozilla-firefox-97-is-now-available-for-download-this-is-whats-new

---

### Main Topic ### 
Is Brave a Good Browser or just a Crypto Scam?


---

### Apps of the Week ###

Matt - Gallery-DL https://github.com/mikf/gallery-dl  

Tyler - terminal.sexy (Find Great Terminal Color Schemes)
