
# Season 6, Episode 15. Episode ID 79
Topic by Tyler




### What have we been up to Linux related this Week? ###

Tyler - I was edumacated on how to theme kdenlive with Kvantum.

Matt – I gave Herbstluftwm a try and I've installed Slackware on hardware. I also used searx now.

---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://betanews.com/2022/04/20/duckduckgo-and-brave-amp-blocking-privacy/ 
Tyler - https://9to5linux.com/canonical-releases-important-ubuntu-kernel-update-to-fix-eight-vulnerabilities

---

### Main Topic ### 
Which is Better Desktop Environments or Window Managers?


---

### Apps of the Week ###

Matt - Gpick 

Tyler - Kvantum
