
# Season 6, Episode 27. Episode ID 91

### What have we been up to Linux related this Week? ###

Tyler - Been developing my game, decided to hold off till next week to show it off. But after a few weeks of development it's in an incredible state.

Matt – Got steam working on Fedora, ended up with the flatpak version with the proton flatpak.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - [Ubuntu Now Available on More RISCV Single Board Computers](https://9to5linux.com/ubuntu-is-now-officially-supported-on-starfives-visionfive-risc-v-single-board-computers) 
Tyler - [New Manjaro ARM 22.08 Release](https://9to5linux.com/manjaro-arm-22-08-released-with-orange-pi-3-and-4-lts-support-linux-kernel-5-19)

---

### Main Topic ### 
Are ARM Macs Good for Linux?


---

### Apps of the Week ###

Matt - DeadCells

Tyler - UnityHub (From The AUR, it's much more stable than the flatpak)
