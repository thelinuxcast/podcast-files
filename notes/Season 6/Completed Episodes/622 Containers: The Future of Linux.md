
# Season 6, Episode 22. Episode ID 86

### What have we been up to Linux related this Week? ###

Tyler - Redid my little mini itx machine to be my gaming & OpenBSD PC

Matt – Considering a switch away from vim. Changing cell carriers. 
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.phoronix.com/scan.php?page=news_item&px=Fedora-37-Unfiltered-Flathubs 
Tyler - https://news.itsfoss.com/gnome-web-extensions-dev/

---

### Main Topic ### 
Are Containers the Future of Linux?


---

### Apps of the Week ###

Matt - TimeShift 

Tyler - Deadside
