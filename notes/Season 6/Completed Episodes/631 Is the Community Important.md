
# Season 6, Episode 31. Episode ID 95

### What have we been up to Linux related this Week? ###

Tyler - Developed issues with Wayland literally right after publicly saying it was going great. So decided to start messing with OpenBSD again ;D YES I FUCKING HOPPED AGAIN! I KNOW! 🤣

Matt – I've been using qtile this entire week. My Python is rusty af.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.tomshardware.com/news/debian-includes-proprietary-code 
Tyler - https://www.theregister.com/2022/10/14/linux_desktop_/

---

### Main Topic ### 
Is the Community Important?


---

### Apps of the Week ###

Matt -  fosstodon

Tyler - lemonbar
