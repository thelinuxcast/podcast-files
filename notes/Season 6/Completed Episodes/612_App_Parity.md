
# Season 6, Episode 12. Episode ID 76
Topic from Matt 


### What have we been up to Linux related this Week? ###

Tyler - I am now a full time OpenBSD user and it's actually really nice. 

Matt – I finished with Gentoo. Couldn't solve the screen tearing issue though I thought I had. Created an rsync script.
---

**Contact Info**

Subscribe at http://thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Subscribe on YouTube - https://youtube.com/thelinuxcast 

Contact Info at https://thelinuxcast.org/contact
---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://www.omgubuntu.co.uk/2022/03/ubuntu-pro-settings-removed-jammy 
Tyler - https://9to5linux.com/shotcut-video-editor-gets-multi-threading-for-all-implicit-video-scaling-and-some-video-filters

---

### Main Topic ### 
Does Linux Need App Parity?


---

### Apps of the Week ###

Matt - Mullvad 

Tyler - DWM Rounded Corners Patch (https://raw.githubusercontent.com/mitchweaver/suckless/master/dwm/patches/mitch-06-rounded_corners-f04cac6d6e39cd9e3fc4fae526e3d1e8df5e34b2.patch)
