# Josh's gripes
## Reporting bugs
 1. [Reporting a bug for a KDE setting is cancerous](https://bugs.kde.org/enter_bug.cgi) as there is no obvious "catchall" option thats sent through a tirage team
 2. IRC and Mailing lists are barriers for younger users who are used to the Github/Gitlab workflow
 3. [Void Linux](https://docs.voidlinux.org/contributing/index.html) wants, code or telemetry nothing else.

## Some projects dont check contributions
 1. Asahi Projects patches for mesa were merged immediately however the keyboard drivers have yet to be merged into libinput
