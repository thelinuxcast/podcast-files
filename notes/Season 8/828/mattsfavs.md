These are the commands and TUI apps that Matt mentioned on the show:

1. Neovim
2. zoxide
3. lsd
4. fastfetch
5. ls
6. wl-copy and xlclip
7. swww
8. borgmatic
9. newsboat
10. pulsemixer
11. yazi
12. Hugo
13. aliases
14. git
