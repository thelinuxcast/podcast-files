# My Nobara thoughts

* I uses the KDE version, though I did take a 10 minute gander at the gnome version


## Installation

Overall, the installation was very vanilla, the kde version uses the calimares installer. It uses btrfs similar to vanilla Fedora, with @ and @home subvolumes.

Surprisingly it felt like the dnf underneath the installer didn't have paralell downloads enabled because the install was fairly slow.

## First impressions

Fairly vanilla kde 6 experience. GTK welcome app pops up to greet. Fans very loud on first boot, and something python related taking up a ton of resources. This does not happen on subsequent boots.

The GTK welcome app is properly themed to match the dark mode that is enabled by default. The app is buried at first boot by the Nobara updater, which is overly confusing and not well designed due to trying to be both user friendly and by showing too much information. Would have been better if it had just opened a terminal and run dnf. 

The welcome app also includes buttons to launch the driver manager, which is also a custom tool. THis detected my builtin GPU and installed the driver. 

There are also links to some "suggested" applications, though not too many and mostly gaming related. Finally there's a button to launch a couple scripts one, which I like, to fix Davinci Resolve. I wish there was an option to install it, that'd have been *awesome*.

Over all the initial impressions were very good, but not overly impressive. It felt like I had just installed a slightly more annoying version of Fedora. I say annoying because of the Updater.

## Updater Issues.

I don't know about my fellow hosts, but it almost proved impossible for me to escape the nobara updater. That thing popped up several times a day while using this thing, which just made it obnoxious. I also noted that for some reason YUM expander is used as a notification and application. I thought YUM was dead, though I might be wrong. 

## Pre Installed Software

Mostly a vanilla Fedora install outside of the gaming elements. Most of the Nobara tweaks aren't actual user facing apps, but instead things like drivers, codecs and such, things that normally take time to do on regular Fedora. 

There were surprises, Inkscape being here, Gimp not being here, and obs-studio actually not being pre installed. I thought that was something that was automatic. But there is an option for it in the welcome app. 

Steam and wine are installed, alongside lutris. More on gaming later.

Flatpak and flathub are enabled by default and are what discover defaults to. ON the Discover font, it is *very* slow, very very slow on first launch. Once it is up and running it seems to do okay, but that first launch took about half a minute maybe more. 

Other than that, it's mostly k-suite stuff, though thankfully, not all of it. 

## Custom tools

Nobar has several custom applications outside of the welcome app. There's the updater, the driver manager, and Nobara tweak tool. IDK if this is different on the Gnome version, but the Tweak tool is very minimal on the tweaks. There are options that allow for better gaming controls and one for automouting external and internal drives. This last one I didn't get to see in action because all the drives I used were in fstab. 

The other app that I used was the Nobara Package manager. This reminded me so much of pamac on Manjaro that I did a double take. It allows you to install and manage software. This includes apps from the Fedora Repos and from flatpak. Though the flatpak integration is lacking. It only shows installed flatpaks and you can't actually search for or install more. That's a bit lame. 

Like pamac, you can install multiple apps at the same time by selcting them and then clicking apply. The app also handles updates, though when it does that, it just opens the nobara updater. These apps should be combined because the Nobara package manager is much better designed. 

Oddly, though, the app closes after you've installed something, so if you needed it to stay open to install more or to continue to browse, you'll have to reopen it. Also lame. 

## Plasma

You can really tell that NObara is a Gnome distro first and foremost. All the tools are gtk. I will say that the dev did a good job of getting them to look good in Plasma. There were no glaring white Adwaita light themes here to be found. If you do go to breeze light, though, the gtk theme changes automatically, which is something not all devs will actually set up. Good job. 

The two apps that are qt based are the updater and the tweak tool. This mismatch was expected but seemed a but jarring when comparing the custom tools. It made it feel like the tools that were gtk were just tacked on even with the nice theming. I also, and I don't have proof of this, but it feels like there might be more tools that are better integrated on the Gnome side. Maybe not true, but I just have a feeling about that. 

## Gaming

I didn't do much gaming, but the one thing that bothered me was that the default steam behavior of not enabling proton was still here. This is a gaming distro, I thought that would be enabled. But maybe it's not even possible, since steam settings only show up once you're logged in.

I decided to install a game that usually doesn't work for me, Sims 4. It worked great on a non-gaming laptop. I don't know what fps or anything like that, but it was very playable with no lag and it launched and seemed full featured. Very impressive. I also tried Cities Skylines, and that was sluggish, but that's a hardware thing. The other game I tried was Spyro and that played but turned my computer into an oven. 

Overall, what little gaming I did seemed great. It worked better out of the box than most distros I've tried.

## Daily Usage

Connecting to an nfs share was easy, and playing back media was a dream when compared to regular Fedora. I installed Davinci resolve on Nobara as well and edited one video. Installing it was just as easy as it was on Bluefin, (not surprising given that they're both Fedora). I did have to move several libraries aroud, but the script that Nobara has seems to fix that. I wish that wasn't burried in the Welcome app. It doesn't make much sense there, since you can't also install Resolve from there. 

Battery life was meh, to be honest. For some reason I found that bluefin (Aurora) to be much better on batter life. I got around 3 hours of regular usage (mostly in Vivaldi and Geany). Less when I fired up Steam, but I suppose that's to be expected. 

Gestures on the default session worked well, though weren't very smooth. At least not as smooth as on Gnome. Probably a waylend thing. 

Plamsa also seemed to be fairly stable. I didn't notice too much in the way of bugs, which is unusual. There is no xorg session, it's wayland or bust.

# Things I Like About It

The gaming experience, what I did of it, was awesome. I think if you want to use Fedora, this is a better choice than Vanilla even if you're not a gamer. Just the fact that the repos are enabled, codecs are installed, and you get access to proprietary drivers, makes it superior to vanilla. 

# Things I Didn't Like

It also seemed like the KDE version was just there so the devs could say they had a KDE version. It's possible that the Gnome experience is just as vanilla, but the KDE version definitely seemed that way. I will say that the little tweaks with the gtk themes made it feel more coheisive than I expected or as is normal on other kde distros.

# Would I use it?

Yes. If I wanted to go for a gaming distro, this would be up there as an option.

# Final thoughts
 
 * Reminds me a LOT of Manjaro. Takes the base distro, makes it easier, has built in tools like Manjaro. It's Uncanny.
 * I would like to spend more time with the gaming aspects of this because I think it'd make for a good distro on my editing pc.
 * I wish that it had snapper set up.
