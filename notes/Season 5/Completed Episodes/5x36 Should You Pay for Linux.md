Idea: Tyler

Topic: Should Distros Charge for Linux?

# Season 5, Episode 36. Episode ID 64

### What have we been up to Linux related this Week? ###

Tyler - I have been enjoying Fedora 35 and my laptop working as expected. However, I am starting to get tired of GNOME.

Matt – I've been moving all of my work into the terminal. I write about 40-70k words a week, and mostly that has all been done in LibreOffice up until now. But now I'm going to trusty vim and working with markdown files. It has been a lot of fun.

---

**Contact Info**

Twitter: @thelinuxcast

Subscribe at http://thelinuxcast.org

Contact us email@thelinuxcast.org

Patreon https://patreon.com/thelinuxcast

Tyler - https://bit.ly/3wk9LNy on Odysee, https://bit.ly/3dbqbjX on YouTube

Subscribe on YouTube - https://youtube.com/thelinuxcast 

---

### NEWS Links (One each) (What's in the News?) ###

Matt - https://9to5linux.com/gamebuntu-app-promises-to-make-gaming-on-ubuntu-painless-for-newcomers
Tyler - https://www.omgubuntu.co.uk/2021/12/zorin-os-16-lite-available-to-download

---

### Main Topic ### 
Should Distros Charge for Linux?


---

### Apps of the Week ###

Matt - Markdown Preview plugin for nvim https://github.com/iamcco/markdown-preview.nvim 

Tyler - EasyEffects https://github.com/wwmm/easyeffects
