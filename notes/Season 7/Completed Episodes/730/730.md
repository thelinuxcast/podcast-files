# Season 7, Episode 30. Episode ID 130
--

What did we do in FOSS This Week?

---

# Topic of the Week (Tyler)

Are corporate backed distros good for the community?

---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/contact/
	
	Steve - https://fosstodon.org/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---


# Thingies of the Week

Josh - [Strawberry](https://flathub.org/apps/org.strawberrymusicplayer.strawberry) the only qt mjusic player I don't immediately hate

Steve - [TripIt](https://www.tripit.com/web)

Tyler - [Bitwarden](https://bitwarden.com)

Matt - [DAvinciBox](https://github.com/zelikos/davincibox) -[Distrobox](https://github.com/89luca89/distrobox) Yes, again. But it's so friggin cool

