# Season 7, Episode 35. Episode ID 135
--

What did we do in FOSS This Week?

---

# Topic of the Week (Tyler)

Are Projects Documentation Getting Better Overall?

---
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/contact/
	
	Steve - https://fosstodon.org/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---


# Nuggies of the Week

Steve - [Joplin](https://joplinapp.org)

Tyler - [Hugo](https://gohugo.io/)

Matt - [Backbone Controller](https://www.amazon.com/Backbone-iOS-Mobile-Gamepad-Controller-Certified/dp/B0CCT2TJW4/ref=sr_1_3?crid=1WHL4ABFQI1CL&keywords=backbone%2Bcontroller&qid=1701543501&sprefix=backbone%2Bcontroller%2Caps%2C100&sr=8-3&ufe=app_do%3Aamzn1.fos.d977788f-1483-4f76-90a3-786e4cdc8f10&th=1)

Josh -
