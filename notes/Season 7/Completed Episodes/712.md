# Season 7, Episode 12. Episode ID 112
--

What did we do in FOSS This Week?

---

## Matt News Links (2 Links)

- [KDE Connect 2.0 Planning For Big Improvements](https://www.phoronix.com/news/KDE-Connect-2.0-Plans)
- [Microsoft is experimenting with a Steam Deck-friendly “handheld mode” for Windows](https://arstechnica.com/gadgets/2023/04/handheld-mode-for-windows-could-make-it-work-better-on-steam-deck-style-pcs/)

## Tyler News Links (2 Links)

- [OpenZFS 2.1.10 Released](https://www.phoronix.com/news/OpenZFS-2.1.10-Released)
- [Google delivers secure open source software packages](https://www.helpnetsecurity.com/2023/04/13/google-secure-open-source-software-packages/)

---
	
	**Contact Info**
	
	Subscribe at http://thelinuxcast.org
	
	Patreon https://patreon.com/thelinuxcast
	
	Subscribe on YouTube - https://youtube.com/thelinuxcast 
	
	Tyler on YouTube - https://youtube.com/ZaneyOG
	
	Josh - https://10leej.com/contact/
	
	Steve - https://youtube.com/@XeroLinux
	
	Email - email@thelinuxcast.org
	
	Contact Info at https://thelinuxcast.org/contact
	
---

## Steve News Links (2 Links)

- [New Flathub Site](https://www.phoronix.com/news/Redesigned-Flathub-Launches)
- [Fedora 38](https://www.phoronix.com/news/Fedora-38-Released)


## Josh News Links (2 Links) - tentative

- [Solus is not a Dead Distro](https://getsol.us/2023/04/18/a-new-voyage/)
	- Josh is working on 16 packages and security/critical updates are being shipped right now! New ISO image is actively being worked on as well.
- [CentOS Sends out a Reminder of EoL for CentOS 7 and CentOS Stream 8](https://www.phoronix.com/news/CentOS-EOL-2023-7-Stream-8)


---
## Apps of the Week

Matt - [SSHFS](https://github.com/libfuse/sshfs) & [scp](https://gist.github.com/JoeyBurzynski/924178a93b44f2307491c75c1c77a9b0)

Steve - [Asterix & Obelix: Slap them All!](https://store.steampowered.com/app/1492310/Asterix__Obelix_Slap_them_All/)

Tyler - [The Chosen](https://watch.angelstudios.com/)

Josh - [KDEConnect](https://kdeconnect.kde.org/)
	- The best desktop tool for integrating devices together, not just phone and computer, but phone to phone, computer to computer, and ect. It's not as reliable as say a samba or NFS share for file transfer, but sync'd notifications, media playback, or even just a quick one tap send a photo via a magic share button. Works pretty good.
